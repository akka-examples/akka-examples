import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import com.typesafe.config.ConfigFactory;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaClustering {

    // -----------------------------------------------------------
    // Actors

    public static class Subscriber extends UntypedActor {

        public Subscriber() {

            ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();

            // subscribe to the topic named "content"
            mediator.tell(new DistributedPubSubMediator.Subscribe("content", getSelf()), getSelf());
        }

        public void onReceive(Object msg) {
            if (msg instanceof String)
                System.out.println("Got message: " + msg.toString());
            else if (msg instanceof DistributedPubSubMediator.SubscribeAck)
                System.out.println("subscribing");
            else
                unhandled(msg);
        }
    }

    public static class Publisher extends UntypedActor {

        // activate the extension
        ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();

        public void onReceive(Object msg) {
            if (msg instanceof String) {
                String in = (String) msg;
                String out = in.toUpperCase();
                mediator.tell(new DistributedPubSubMediator.Publish("content", out), getSelf());
            } else {
                unhandled(msg);
            }
        }

    }

    // end of actors
    // -----------------------------------------------------------

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("AkkaCluster", ConfigFactory.parseString(
                    "akka {\n" +
                            "  actor.provider = \"akka.cluster.ClusterActorRefProvider\"\n"+
                            "  remote.netty.tcp {\n"+
                            "    hostname = \"127.0.0.1\"\n"+
                            "    port = 0" +
                            "  }\n"+
                            "  cluster.seed-nodes = [\"akka.tcp://AkkaCluster@127.0.0.1:2551\"]\n"+
                            "}"));

            // -----------------------------------------------------------
            // subcribers
            // -----------------------------------------------------------

            system.actorOf(Props.create(Subscriber.class), "subscriber1");
            system.actorOf(Props.create(Subscriber.class), "subscriber2");
            system.actorOf(Props.create(Subscriber.class), "subscriber3");

            // give a second to subscribe
            Thread.sleep(1000);

            // -----------------------------------------------------------
            // publishers
            // sent the message to someone in the cluster
            // -----------------------------------------------------------

            //somewhere else
            ActorRef publisher = system.actorOf(Props.create(Publisher.class), "publisher");

            // after a while the subscriptions are replicated
            publisher.tell("hello", null);

            // -----------------------------------------------------------
            // terminate
            // -----------------------------------------------------------

            // give a second to process
            Thread.sleep(1000);

            // shutdown ActorSystem
            system.terminate();

            Await.ready(system.whenTerminated(), Duration.create(1, TimeUnit.MINUTES));


        }
        catch (InterruptedException ex) {
            System.out.println("Terminated");
            ex.printStackTrace();
        }
        catch (TimeoutException ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }
    }

}
