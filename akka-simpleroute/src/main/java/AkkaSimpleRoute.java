import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaSimpleRoute {

    // -----------------------------------------------------------
    // Messages
    public static final class Work implements Serializable {
        private static final long serialVersionUID = 1L;
        public final String payload;
        public Work(String payload) {
            this.payload = payload;
        }
    }
    // end of messages
    // -----------------------------------------------------------

    // -----------------------------------------------------------
    // Actors
    public static class Worker extends UntypedActor {

        public void onReceive(Object msg) {
            if (msg instanceof Work) {
                System.out.println(((Work) msg).payload + ":" + this.toString());
            }
        }

    }

    public static class Master extends UntypedActor {

        Router router;
        {
            List<Routee> routees = new ArrayList<Routee>();
            for (int i = 0; i < 5; i++) {
                ActorRef r = getContext().actorOf(Props.create(Worker.class));
                getContext().watch(r);
                routees.add(new ActorRefRoutee(r));
            }
            router = new Router(new RoundRobinRoutingLogic(), routees);
        }

        public void onReceive(Object msg) {
            if (msg instanceof Work) {
                router.route(msg, getSender());
            } else if (msg instanceof Terminated) {
                router = router.removeRoutee(((Terminated) msg).actor());
                ActorRef r = getContext().actorOf(Props.create(Worker.class));
                getContext().watch(r);
                router = router.addRoutee(new ActorRefRoutee(r));
            }
        }
    }
    // end of actors
    // -----------------------------------------------------------

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("akka_sample");

            // Create the 'storeActor' actor
            final ActorRef masterActor = system.actorOf(Props.create(Master.class), "master");

            // -----------------------------------------------------------
            // interaction with Actor
            // -----------------------------------------------------------

            // Send (tell) state to StoreActor
            masterActor.tell(new Work("akka"), ActorRef.noSender());


        } catch (Exception ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }
    }

}
