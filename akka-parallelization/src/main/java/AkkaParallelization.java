import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by rht on 10/5/2016.
 */
public class AkkaParallelization {

    // -----------------------------------------------------------
    // Messages

    @Getter
    @Builder
    public static class FinishMessage {

        private long elapsedTime;

    }

    public static class DoWorkMessage {}

    public static class DoCalcMessage {}

    @Getter
    @Builder
    public static class ResultMessage {

        private BigInteger bigInt;

    }

    // -----------------------------------------------------------

    // -----------------------------------------------------------
    // Actors

    @Setter
    @Getter
    public static class MasterActor extends UntypedActor {

        private int messages;

        private ActorRef workerRouter;

        private ActorRef initiator;

        private List list;

        private long msecBefore;

        public MasterActor(int messages) {
            this.messages = messages;
            this.list = new ArrayList(messages);
            this.workerRouter = this.getContext().actorOf(WorkerActor.createWorker().withRouter(new RoundRobinPool(AkkaParallelization.threads)), "workerRouter");
        }

        public static Props createMaster() {
            return Props.create(MasterActor.class, AkkaParallelization.messages);
        }

        @Override
        public void onReceive(Object message) {
            if (message instanceof DoCalcMessage) {
                setInitiator(getSender());
                processMessages();
            } else if (message instanceof ResultMessage) {
                list.add(((ResultMessage) message).getBigInt());
                if (list.size() == messages) {
                    end();
                }
            } else {
                unhandled(message);
            }
        }

        private void end() {
            // response
            getInitiator().tell(new FinishMessage(System.currentTimeMillis() - msecBefore), getSelf());
        }

        private void processMessages() {
            msecBefore = System.currentTimeMillis();
            for (int i = 0; i < messages; i++) {
                workerRouter.tell(new DoWorkMessage(), getSelf());
            }
        }

    }

    public static class WorkerActor extends UntypedActor {

        @Override
        public void onReceive(Object message) {
            if (message instanceof DoWorkMessage) {
                getSender().tell(new ResultMessage(CalculateFactorial.calculate()), getSelf());
            } else
                unhandled(message);
        }

        public static Props createWorker() {
            return Props.create(WorkerActor.class);
        }

    }

    // end of actors
    // -----------------------------------------------------------

    static int messages = 100000;

    static int threads = 8;

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // Measurement of Vanila Java (no threads)
            // -----------------------------------------------------------

            long msecBefore = System.currentTimeMillis();

            List list = new ArrayList<BigInteger>(messages);

            for (int i = 0; i < messages; i++) {
                list.add(CalculateFactorial.calculate());
            }

            System.out.println("Time (msec): " + (System.currentTimeMillis() - msecBefore));

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("AkkaParallelization");

            // Create the "actor-in-a-box" wrapper
            final Inbox inbox = Inbox.create(system);

            // -----------------------------------------------------------
            // Time measurement of Akka Java (with parallelization from the box)
            // -----------------------------------------------------------

            ActorRef master = system.actorOf(MasterActor.createMaster(), "master");

            // send DoCalcMessage and wait for the end
            inbox.send(master, new DoCalcMessage());

            FinishMessage result = (FinishMessage)inbox.receive(Duration.create(5, TimeUnit.MINUTES));

            System.out.println("Akka. Elapsed time (msec): " + result.getElapsedTime());

            // -----------------------------------------------------------
            // terminate
            // -----------------------------------------------------------

            // shutdown ActorSystem
            system.terminate();

            Await.ready(system.whenTerminated(), Duration.create(1, TimeUnit.MINUTES));


        }
        catch (InterruptedException ex) {
            System.out.println("Terminated");
            ex.printStackTrace();
        }
        catch (TimeoutException ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }
    }

}
