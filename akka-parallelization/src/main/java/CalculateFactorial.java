import java.math.BigInteger;

/**
 * Created by rht on 10/7/2016.
 */
public class CalculateFactorial {

    public static BigInteger calculate() {

        BigInteger fact = BigInteger.valueOf(1);

        for (int i = 1; i <= 739; i++)
            fact = fact.multiply(BigInteger.valueOf(i));

        return fact;
    }

}

