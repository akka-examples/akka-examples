import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.actor.TypedActor;
import akka.actor.TypedProps;
import akka.actor.UntypedActor;
import akka.dispatch.Futures;
import akka.japi.Creator;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaTypedActors {

    // -----------------------------------------------------------
    // Messages

    public static class StopMessage implements Serializable {}

    public static class DoneMessage implements Serializable {}

    // -----------------------------------------------------------

    // -----------------------------------------------------------
    // Untyped Actor

    public static class StoreActor extends UntypedActor {
        public void onReceive(Object message) {
        }
    }
    // end of actors
    // -----------------------------------------------------------

    // -----------------------------------------------------------
    // Typed Actor

    public interface IStoreTyped {

        //fire-forget
        void setName(String name);

        //non-blocking send-request-reply
        Future<String> getNameAsync();

        //blocking send-request-reply
        String getNameBlocking();

    }

    public static class StoreTyped implements akka.actor.TypedActor.Receiver, IStoreTyped {

        private String name;

        public StoreTyped() {
            this.name = "default";
        }

        public StoreTyped(String name) {
            this.name = name;
        }

        // ---------------------------------
        // Gracefull shutdown
        // ---------------------------------

        public void onReceive(Object message, ActorRef actorRef) {
            System.out.println("onReceive(), message: " + message.toString());
            if (message instanceof StopMessage) {
                // do something to stop
                TypedActor.context().sender().tell(new DoneMessage(), TypedActor.context().self());
            }
        }

        // ---------------------------------

        public void setName(String name) {
            this.name = name;
        }

        public Future<String> getNameAsync() {
            return Futures.successful(name);
        }

        public String getNameBlocking() {
            return this.name;
        }



    }
    // end of actors
    // -----------------------------------------------------------

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("akka_sample");

            // Create the 'storeActor' actor
            final ActorRef storeActor = system.actorOf(Props.create(StoreActor.class), "storeActor");

            // -----------------------------------------------------------
            // AkkaTypedActors
            // -----------------------------------------------------------

            // Returns whether the reference is a Typed Actor Proxy or not
            System.out.println("Is storeActor typed of TypedActor: " + TypedActor.get(system).isTypedActor(storeActor));

            // Instanciate TypedActor
            IStoreTyped storeTypedActor =
                    TypedActor.get(system).typedActorOf(
                            new TypedProps<StoreTyped>(IStoreTyped.class, StoreTyped.class));

            IStoreTyped storeTypedActorWithConstructor =
                    TypedActor.get(system).typedActorOf(
                            new TypedProps<StoreTyped>(IStoreTyped.class,
                                    new Creator<StoreTyped>() {
                                        public StoreTyped create() { return new StoreTyped("changed"); }
                                    }),
                            "name");

            // Returns whether the reference is a Typed Actor Proxy or not
            System.out.println("Is storeTypedActor typed of TypedActor: " + TypedActor.get(system).isTypedActor(storeTypedActor));

            // Returns whether the reference is a Typed Actor Proxy or not
            System.out.println("Is storeTypedActorWithConstructor typed of TypedActor: " + TypedActor.get(system).isTypedActor(storeTypedActorWithConstructor));

            // -----------------------------------------------------------
            // call of methods
            // -----------------------------------------------------------

            System.out.println("storeTypedActor name: " + storeTypedActor.getNameBlocking());

            System.out.println("storeTypedActorWithConstructor name: " + storeTypedActorWithConstructor.getNameBlocking());

            String name = Await.result(storeTypedActor.getNameAsync(), Duration.create(5, TimeUnit.SECONDS));

            System.out.println("storeTypedActor name with Feature: " + name);

            // -----------------------------------------------------------
            // AKKA termination
            // -----------------------------------------------------------

            // Create the "actor-in-a-box" wrapper
            final Inbox inbox = Inbox.create(system);

            ActorRef justForStop = TypedActor.get(system).getActorRefFor(storeTypedActor);

            // send stop and wait for 5 seconds
            inbox.send(justForStop, new StopMessage());
            inbox.receive(Duration.create(5, TimeUnit.SECONDS));
            TypedActor.get(system).stop(justForStop);

            // OR without waiting, async stop
            TypedActor.get(system).stop(storeTypedActorWithConstructor);

            // shutdown ActorSystem
            system.terminate();

            Await.ready(system.whenTerminated(), Duration.create(1, TimeUnit.MINUTES));

        } catch (Exception ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }
    }

}
