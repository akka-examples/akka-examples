import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.util.Arrays;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaStreams {

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("akka_sample");

            final Materializer mat = ActorMaterializer.create(system);

            // -----------------------------------------------------------
            // streaming
            // -----------------------------------------------------------

            // source of data
            final Source<Integer, NotUsed> source =
                    Source.from(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

            // note that the Future is scala.concurrent.Future
            final Sink<Integer, CompletionStage<Integer>> sink =
                    Sink.<Integer, Integer> fold(0, (aggr, next) -> aggr + next);

            // connect the Source to the Sink, obtaining a RunnableFlow
            final RunnableGraph<CompletionStage<Integer>> runnable =
                    source.toMat(sink, Keep.right());

            // materialize the flow
            final CompletionStage<Integer> sum = runnable.run(mat);

            // print the result
            sum.thenAcceptAsync(val -> System.out.println("The total is: " + val),
                    system.dispatcher());

            // -----------------------------------------------------------
            // terminate
            // -----------------------------------------------------------

            // shutdown ActorSystem
            system.terminate();

            Await.ready(system.whenTerminated(), Duration.create(1, TimeUnit.MINUTES));
        }
        catch (InterruptedException ex) {
            System.out.println("Terminated");
            ex.printStackTrace();
        }
        catch (TimeoutException ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }

    }

}
