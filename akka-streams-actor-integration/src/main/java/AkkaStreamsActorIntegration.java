import static com.sun.webkit.graphics.WCRenderQueue.MAX_QUEUE_SIZE;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.actor.AbstractActorSubscriber;
import akka.stream.actor.ActorSubscriberMessage;
import akka.stream.actor.MaxInFlightRequestStrategy;
import akka.stream.actor.RequestStrategy;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaStreamsActorIntegration {

    // -----------------------------------------------------------
    // Messages

    public static class Request {}

    @Builder
    @Setter
    @Getter
    public static class Response {
        private int result;
    }

    // end of messages
    // -----------------------------------------------------------


    // -----------------------------------------------------------
    // Actors

    public static class NumberProcessing extends AbstractActorSubscriber {

        private int total = 0;

        public static Props props() { return Props.create(NumberProcessing.class); }

        @Override
        public RequestStrategy requestStrategy() {
            return new MaxInFlightRequestStrategy(MAX_QUEUE_SIZE) {
                @Override
                public int inFlightInternally() {
                    return 0; // no limitation
                }
            };
        }

        public NumberProcessing() {
            receive(ReceiveBuilder.
                match(ActorSubscriberMessage.OnNext.class, on -> on.element() instanceof Integer,
                    onNext -> {
                        total += (int)onNext.element();
                    }).
                match(Request.class, reply -> {
                    sender().tell(Response.builder().result(total).build(), self());
                }).
                build());
        }
    }

    // end of actors
    // -----------------------------------------------------------

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("akka_streams");

            // Create the "actor-in-a-box" wrapper
            final Inbox inbox = Inbox.create(system);

            final Materializer mat = ActorMaterializer.create(system);

            // -----------------------------------------------------------
            // streaming
            // -----------------------------------------------------------

            // source of data
            final Source<Integer, NotUsed> source =
                    Source.from(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

            // destination of data processing, here we will calculate all incoming values from the Source
            final Sink<Integer, ActorRef> sink =
                    Sink.actorSubscriber(NumberProcessing.props());

            // execute & materialize
            final ActorRef numberProcessing = source.runWith(sink, mat);

            // give a time for NumberProcessing to complete its job (akka is asynchronous)
            Thread.sleep(1000);

            // Send request to numberProcessing
            inbox.send(numberProcessing, new Request());

            // Wait 5 seconds for the reply with the Response message
            final Response response = (Response) inbox.receive(Duration.create(5, TimeUnit.SECONDS));

            System.out.println("The total is: " + response.getResult());

            // -----------------------------------------------------------
            // terminate
            // -----------------------------------------------------------

            // shutdown ActorSystem
            system.terminate();

            Await.ready(system.whenTerminated(), Duration.create(1, TimeUnit.MINUTES));
        }
        catch (InterruptedException ex) {
            System.out.println("Terminated");
            ex.printStackTrace();
        }
        catch (TimeoutException ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }

    }

}
