import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.DeadLetter;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.actor.UntypedActor;
import scala.concurrent.duration.Duration;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaDeadLetter {

    // -----------------------------------------------------------
    // Messages
    public static class StopMessage implements Serializable {}
    public static class DoneMessage implements Serializable {}
    // end of messages
    // -----------------------------------------------------------


    // -----------------------------------------------------------
    // Actors
    public static class DeadLetterActor extends UntypedActor {
        public void onReceive(Object message) {
            if (message instanceof DeadLetter) {
                System.out.println(message);
            }
        }
    }
    public static class StoppableActor extends UntypedActor {

        public void onReceive(Object message) {
            if (message instanceof StopMessage) {
                System.out.println("Stopping of " + this.getClass().toString());
                context().stop(getSelf());
                getSender().tell(new DoneMessage(), getSelf());
            }
        }

        @Override
        public void postStop() {
            System.out.println("PostStop for " + this.getClass().toString());
        }


    }
    // Actors
    public static class StoreActor extends StoppableActor {

        private String state = "";

        public void onReceive(Object message) {
            super.onReceive(message);
        }
    }

    // end of actors
    // -----------------------------------------------------------

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            final ActorSystem system = ActorSystem.create("DeadLetters");

            // Create the 'storeActor' actor
            final ActorRef storeActor = system.actorOf(Props.create(StoreActor.class), "storeActor");

            // Create the "actor-in-a-box" wrapper
            final Inbox inbox = Inbox.create(system);

            final ActorRef deadLetterActor = system.actorOf(Props.create(DeadLetterActor.class));

            system.eventStream().subscribe(deadLetterActor, DeadLetter.class);

            // -----------------------------------------------------------
            // Stopping all actors softly
            // -----------------------------------------------------------

            // send stop and wait for 5 seconds
            inbox.send(storeActor, new StopMessage());
            inbox.receive(Duration.create(5, TimeUnit.SECONDS));

            // what if we will try to deliver the message to the stopped Actor ?
            inbox.send(storeActor, new StopMessage());

        } catch (Exception ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }
    }

}
