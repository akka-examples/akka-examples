import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Props;
import akka.actor.UntypedActor;
import lombok.Builder;
import scala.concurrent.duration.Duration;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by rht on 10/5/2016.
 */

public class AkkaBegining {

    // -----------------------------------------------------------
    // Messages
    public static class GetStateMessage implements Serializable {}

    @Builder
    public static class SetStateMessage implements Serializable {
        private final String state;
    }
    @Builder
    public static class StateMessage implements Serializable {
        private final String message;
    }
    // end of messages
    // -----------------------------------------------------------

    // -----------------------------------------------------------
    // Actors
    public static class StoreActor extends UntypedActor {

        private String state = "";

        public void onReceive(Object message) {

            if (message instanceof SetStateMessage) {
                System.out.println("StoreActor:SetStateMessage");
                this.state = ((SetStateMessage) message).state;
            }

            // Send the current greeting back to the sender
            else if (message instanceof GetStateMessage) {
                System.out.println("StoreActor:GetStateMessage");
                getSender().tell(new StateMessage(this.state), getSelf());
            }

            else unhandled(message);
        }
    }
    public static class StatePrinterActor extends UntypedActor {

        public void onReceive(Object message) {
            if (message instanceof StateMessage) {
                System.out.println("StatePrinterActor:StateMessage");
                System.out.println(((StateMessage) message).message);
            }
        }

    }
    // end of actors
    // -----------------------------------------------------------

    public static void main(String[] args) {
        try {

            // -----------------------------------------------------------
            // AKKA initialization
            // -----------------------------------------------------------

            // Create the 'akka_sample' actor system
            final ActorSystem system = ActorSystem.create("akka_sample");

            // Create the 'storeActor' actor
            final ActorRef storeActor = system.actorOf(Props.create(StoreActor.class), "storeActor");

            // Create the "actor-in-a-box" wrapper
            final Inbox inbox = Inbox.create(system);


            // -----------------------------------------------------------
            // interaction with Actor
            // -----------------------------------------------------------

            // Send (tell) state to StoreActor
            storeActor.tell(new SetStateMessage("akka"), ActorRef.noSender());

            // Get (ask) state of StoreActor
            // Reply should go to the "actor-in-a-box"
            inbox.send(storeActor, new GetStateMessage());

            // Wait 5 seconds for the reply with the StateMessage message
            final StateMessage stateMessage = (StateMessage) inbox.receive(Duration.create(5, TimeUnit.SECONDS));


            // -----------------------------------------------------------
            // printing the state by StatePrinterActor
            // -----------------------------------------------------------

            // Create the 'statePrinterActor' actor
            final ActorRef statePrinterActor = system.actorOf(Props.create(StatePrinterActor.class), "statePrinterActor");

            // Set (tell) state of StoreActor
            statePrinterActor.tell(stateMessage, ActorRef.noSender());


        } catch (TimeoutException ex) {
            System.out.println("Got a timeout waiting for reply from an actor");
            ex.printStackTrace();
        }
    }

}
